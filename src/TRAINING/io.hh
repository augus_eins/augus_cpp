//
// Created by AUGUS on 2021/8/9.
//

#ifndef AUGUS_CPP_IO_HH
#define AUGUS_CPP_IO_HH

#include <vector> // for std::vector
#include <iostream> // for std::cout
#include <fstream>
#include <valarray> // for std::valarray std::max_element std::min_element std::sort
#include <set> // for std::set
#include <string> // for memset
#include <map> // for std::map
#include <stack> // for std::stack
#include <memory>

#include "augus/lib_func.hh"

namespace io_test {
void string2binary();
}//namespace io_test

#endif //AUGUS_CPP_IO_HH

//
// Created by AUGUS on 2021/5/31.
//

#include "gtest/gtest.h"

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    int ret = RUN_ALL_TESTS();

    return ret;
}
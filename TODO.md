
#### TOP
一些散记
* 如果大家想看某一个领域的论文，可以尝试搜一下awesome+*，比如awesome domain adaptaion，说不定有惊喜
* <a href="https://www.macrumors.com/roundup/macbook-pro/" title="新款apple预测" target="_blank">Apple新款预测</a>
* <a href="https://www.maigoo.com/" title="一个神奇的排行榜网站" target="_blank">一个神奇的排行榜网站</a>
* <a href="https://hk.v2ex.com/" title="一个程序员社区似乎需要科学工具" target="_blank">一个程序员社区</a>



### 一些教程

- [ ] <a href="https://markdown.com.cn/extended-syntax/" title="扩展语法">markdown高级教程</a> 
- [ ] <a href="https://markdown.com.cn/basic-syntax/" title="基本语法">markdown初级教程</a>

### 书籍
- [ ] Linux内核源代码情景分析
- [ ] 实战linux socket编程

### NICE WEB
- [ ] <a href="https://datatracker.ietf.org/doc/html/rfc6265" title="HTTP State Management Mechanism">HTTP State Management Mechanism</a>
- [ ] <a href="https://github.com/zmap/zmap" target="_blank">快速扫描端口：ZMap is a fast single packet network scanner designed for Internet-wide network surveys.</a>
- [ ] [计算机网络协议全面总结](./doc/web_page/NetworkProtocol.md)
- [ ] <a href="https://www.cnblogs.com/coderzh/" title="玩转Google开源C++单元测试框架Google Test系列" target="_blank">玩转Google开源C++单元测试框架Google Test系列</a>
- [ ] <a href="http://caibaojian.com/interview-map/frontend/" title="js前端面试之道" target="_blank">js前端面试之道</a>


##### BERT
- [ ] [一文读懂BERT(原理篇)](./doc/web_page/BERT-Principles.md) 
- [ ] [The Illustrated Transformer](./doc/web_page/TheIllustratedTransformer.md)
- [ ] <a href="https://zhuanlan.zhihu.com/p/303080210" target="_blank"><img src="https://img.shields.io/badge/-由浅入深transformer-F08080" alt="由浅入深transformer"/> </a>
- [ ] <a href="https://zhuanlan.zhihu.com/p/68446772" target="_blank"><img src="https://img.shields.io/badge/-Bert时代的创新（应用篇）：Bert在NLP各领域的应用进展-DB7093" alt="Bert时代的创新（应用篇）：Bert在NLP各领域的应用进展"/> </a>
- [ ] <a href="https://zhuanlan.zhihu.com/p/48612853" target="_blank"><img src="https://img.shields.io/badge/-词向量之BERT-DDA0DD" alt="词向量之BERT"/> </a>



## 学习计划列表

- [ ] `时序图`绘画练习，工具下载安装使用



### 20210510

thread study<br>

<ul>
  <li>线程安全、线程同步与互斥关系、线程如何通信、与进程的关系如何</li>
  <li>研读 C++ Primer 线程部分</li>
  <li>智能指针 并发处理 线程分配</li>
  <li> <a href="https://www.jianshu.com/p/8c1bb012d5f8" target="_blank"> <img src="https://img.shields.io/badge/atomic-🤏研究一下-red" alt="atomic"/> </a></li>
  <li> <a href="https://zhuanlan.zhihu.com/p/107092432" target="_blank"> <img src="https://img.shields.io/badge/atomic-🤏研究2下-red" alt="atomic"/> </a> </li>
  <li> <a href="https://www.cnblogs.com/wxquare/p/4759020.html" target="_blank"> <img src="https://img.shields.io/badge/autoptr-🤏研究一下-r" alt="autoptr"/> </a> </li>
  <li> <a href="https://www.runoob.com/w3cnote/cpp-std-thread.html" target="_blank">  <img src="https://img.shields.io/badge/std::thread-🤏研究一下-yellowgreen" alt="std::thread"/> </a> </li>
  <li> <a href="https://www.cnblogs.com/haippy/p/3236136.html " target="_blank">  <img src="https://img.shields.io/badge/std::thread-🤏研究2下-yellowgreen" alt="std::thread"/> </a></li>
</ul>

### 20210512

<a href="https://blog.csdn.net/hepangda/article/details/85236693 " target="_blank"><img src="https://img.shields.io/badge/asio lib-🤏-r" alt="asio lib"/></a>
<a href="https://think-async.com/Asio/asio-1.18.1/doc/asio/overview/core/threads.html " target="_blank"><img src="https://img.shields.io/badge/Asio C++ Library-🤏-r" alt="Asio C++ Library"/></a>



<a href="https://www.informit.com/store/c-plus-plus-primer-9780321714114" target="_blank"><img src="https://img.shields.io/badge/C++ primer source code-🤏-r" alt="C++ primer source code"/></a>
<a href="https://www.cnblogs.com/sunchaothu/p/10389842.html" target="_blank"><img src="https://img.shields.io/badge/单例剖析-🤏-r" alt="单例剖析"/></a>


### 20210601

<a href=" https://blog.csdn.net/andy_5826_liu/article/details/85309634" target="_blank"><img src="https://img.shields.io/badge/字符串拼接，json对象组装-🤏-r" alt="字符串拼接，json对象组装"/></a>



<datalist id="default-colors">
<option value="brightgreen"></option>
<option value="green"></option>
<option value="yellowgreen"></option>
<option value="yellow"></option>
<option value="orange"></option>
<option value="red"></option>
<option value="lightgrey">
</option><option value="blue"
></option></datalist>

